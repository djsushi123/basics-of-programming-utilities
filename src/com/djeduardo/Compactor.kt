package com.djeduardo

import java.io.File

fun main() {

    // Read the file and remove all the unnecessary parts
    val text = File("src/com/djeduardo/Main.java")
        .readLines()
        .asSequence()
        .map { it.trim() }
        .filter { !it.startsWith("//") }
        .filter { !it.startsWith("package") }
        .filter { !it.startsWith("public class Main") }
        .filter { it != "}" }
        .joinToString("")

    println("The snippet length is ${text.length} characters long.")

    // Read the README.md file
    val readme = File("README.md").readLines().toMutableList()

    // Search through the lines and find where the Java code block is
    // We do +1 at the end because we want to get the index of the next line
    val snippetIndex = readme.indexOfFirst { it.startsWith("```java") } + 1

    // replace the old snippet with the new one
    readme[snippetIndex] = text

    // Write the changes back into README.md
    File("README.md").writeText(readme.joinToString("\n"))
}