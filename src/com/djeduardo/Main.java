package com.djeduardo;

public class Main {
    
    // System.out.println() overloads
    public static void println(String text) { System.out.println(text); }

    public static void println(int text) { System.out.println(text); }

    public static void println(long text) { System.out.println(text); }

    public static void println(float text) { System.out.println(text); }

    public static void println(double text) { System.out.println(text); }

    public static void println(byte text) { System.out.println(text); }

    public static void println(boolean text) { System.out.println(text); }

    public static void println(char text) { System.out.println(text); }

    // System.out.print() overloads
    public static void print(String text) { System.out.print(text); }

    public static void print(int text) { System.out.print(text); }

    public static void print(long text) { System.out.print(text); }

    public static void print(float text) { System.out.print(text); }

    public static void print(double text) { System.out.print(text); }

    public static void print(byte text) { System.out.print(text); }

    public static void print(boolean text) { System.out.print(text); }

    public static void print(char text) { System.out.print(text); }

    // printArray()
    public static <T> void printArray(T[] array) {
        for (T t : array) {
            if (t != null) {
                System.out.println(t);
            }
        }
    }

    public static void printArray(int[] array){
        for (int j : array) {
            System.out.println(j);
        }
    }

    public static void printArray(long[] array){
        for (long v : array) {
            System.out.println(v);
        }
    }

    public static void printArray(float[] array){
        for (float v : array) {
            System.out.println(v);
        }
    }

    public static void printArray(double[] array){
        for (double v : array) {
            System.out.println(v);
        }
    }

    public static void printArray(byte[] array){
        for (byte c : array) {
            System.out.println(c);
        }
    }

    public static void printArray(boolean[] array){
        for (boolean c : array) {
            System.out.println(c);
        }
    }

    public static void printArray(char[] array){
        for (char c : array) {
            System.out.println(c);
        }
    }

    // invertArray()
    public static <T> T[] invertArray(T[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            T j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static int[] invertArray(int[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            int j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static long[] invertArray(long[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            long j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static float[] invertArray(float[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            float j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static double[] invertArray(double[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            double j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static byte[] invertArray(byte[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            byte j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static boolean[] invertArray(boolean[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            boolean j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }

    public static char[] invertArray(char[] array) {
        for (int i = 0; i < (array.length / 2); i++) {
            char j = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = j;
        }
        return array;
    }
}


